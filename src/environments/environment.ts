// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr: false,
    // Local
    // endpoint: 'http://219.90.67.69:8011/TORNADO_API/',
    endpoint: 'http://localhost:5000/',

    HubURL: 'http://219.90.67.69:8011/TORNADO_API/message',
    // endpoint: 'http://192.168.1.41/RBS_API/',
    // HubURL: 'http://192.168.1.41/RBS_API/message',
    ReportURL: 'http://192.168.1.41:8445/SSFBMIS_APP/ViewReport.aspx'


};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
