import { Component, HostBinding, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector   : 'fuse-nav-horizontal-item',
    templateUrl: './item.component.html',
    styleUrls  : ['./item.component.scss']
})
export class FuseNavHorizontalItemComponent
{
    @HostBinding('class')
    classes = 'nav-item';

    @Input()
    item: any;

    /**
     * Constructor
     */
    constructor( private toastr: ToastrService )
    {

    }
    Demo(){
        this.toastr.error('For Demo This Functionality Is Locked');
      }
}
