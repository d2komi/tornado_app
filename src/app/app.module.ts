import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { RequestOptions } from '@angular/http';
import { HttpModule, XHRBackend } from '@angular/http';
import { ApiHandler } from './AUTH/providers/api-handler.service';
import { AuthService } from './AUTH/providers/auth.service';
import { UserService } from './AUTH/providers/user.service';
import { RoleGuard } from './AUTH/guards/role.guard';
import { AuthGuard } from './AUTH/guards/auth.guard';
import { LoginModule } from './main/AuthModules/login/login.module';
import { DashboardModule } from './main/Dashboard/dashboard.module';
import { Error404Module } from './main/AuthModules/404/error-404.module';
import { SingletonModule } from './main/Singleton/Singleton.module';
import { NgxUiLoaderModule, NgxUiLoaderConfig, SPINNER, POSITION, PB_DIRECTION } from 'ngx-ui-loader';
import { homeService } from './main/Dashboard/home/home.service';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeDbService } from './main/Dashboard/fake-db/fake-db.service';
const ngxUiLoaderConfig: NgxUiLoaderConfig ={
    "blur": 5,
    "fgsColor": "White",
    "fgsPosition": "center-center",
    "fgsType": SPINNER.rectangleBounce,
    "gap": 10,
    "logoPosition": "center-center",
    "logoSize": 10,
    "logoUrl": "",
    "overlayColor": "rgba(40, 40, 40, 0.8)",
    "pbColor": "#00ACC1",
    "pbDirection": "ltr",
    "pbThickness": 1,
    "hasProgressBar": true,
    "text": "",
    "textColor": "#FFFFFF",
    "textPosition": "center-center",
    "threshold": 500,


};
const appRoutes: Routes = [
    {
        path: 'login',
        redirectTo: 'login'
    },
    {
        path: '',
        redirectTo: 'dashboard', pathMatch: 'full'
        // canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'errors/error-404', pathMatch: 'full'
        // canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    },

    // { path: '', redirectTo: 'sample', pathMatch: 'full' }
];
export function handlerFunc(backend: XHRBackend, defaultOptions: RequestOptions) {
    return new ApiHandler(backend, defaultOptions);
}
@NgModule({
    declarations: [
        AppComponent       

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        TranslateModule.forRoot(),
        SingletonModule.forRoot(),
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        // SampleModule,
        LoginModule,
        DashboardModule,
        Error404Module
    ],
    providers: [
        AuthGuard,
        RoleGuard,
        UserService,
        homeService,
        AuthService,
        {
            provide: ApiHandler,
            useFactory: handlerFunc, // (backend: XHRBackend, defaultOptions: RequestOptions) => new ApiHandler(backend, defaultOptions),
            deps: [XHRBackend, RequestOptions]
        }],
    exports: [],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
