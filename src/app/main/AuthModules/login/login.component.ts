import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from '../../../AUTH/providers/auth.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    message: string;
    loading: boolean;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(private _authService: AuthService, private _router: Router,private ngxService: NgxUiLoaderService,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.ngxService.stop();
        this.ngxService.stopAll();
        this.message = this._authService.message;
        // this._authService.clear();
        if (this._authService.isAuthenticated()) {
            // let's check if the token will expire soon
            if (!this._authService.shouldIGetToken()) {
                const to: string = this._authService.getRedirectUrl() || '/dashboard';
                this._router.navigate([to]);
            }
        }

        this.createForm();
    }

    createForm(): void {
        this.loginForm = this._formBuilder.group({
            Username: ['', [Validators.required]],
            password: ['', Validators.required]
        });
    }
    submitted() {
        if (this.loginForm.invalid) {
            return;
        }
        this.ngxService.start();

        // set our login loading indicator to show we have started the server communication
        this.loading = true;
        this._authService
            .authenticate(this.loginForm.value.Username, this.loginForm.value.password)
            .subscribe((data) => {
                this.loading = false; // hide our loading indicator
                // navigate back to our redirect url if empty goto our dashboard
                this.ngxService.stop();
                const authData = JSON.parse(data);
           
                if (authData.results.status !== -1) {
                    const to: string = this._authService.getRedirectUrl() || '/dashboard';
                    this._router.navigate([to]);
                } else {
                    this.message = authData.results.msg;
                }

            },
                error => {
                    this.loading = false;
                    this.message = error;
                   this.ngxService.stop();
                    console.error('auth error', error);
                }
            );
    }
}
