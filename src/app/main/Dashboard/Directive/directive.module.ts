import { NgModule } from "@angular/core";
import { NumberOnly } from "./Validation/NumberOnly.directive";
import { RestrictInputDirective } from "./Validation/RestrictInput.directive";


@NgModule({
    declarations: [ NumberOnly, RestrictInputDirective ],
    exports: [ NumberOnly, RestrictInputDirective ]
  })
  export  class directiveModule {}
  