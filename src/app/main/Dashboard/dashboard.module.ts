
import { ToastrModule } from 'ngx-toastr';
import { FuseSharedModule } from '@fuse/shared.module';
import { AuthGuard } from '../../AUTH/guards/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import {
    MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatRadioModule,
    MatStepperModule, MatDatepickerModule, MatCardModule, MatAutocompleteModule, DateAdapter
} from '@angular/material';
import { OrderModule } from 'ngx-order-pipe';
import {
    MatCheckboxModule, MatSelectModule, MatIconModule, MatDialogModule, MatMenuModule,
    MatTabsModule, MatToolbarModule, MatDividerModule, MatButtonModule, MatExpansionModule, MatChipsModule, MatSlideToggleModule, MatInputModule
} from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';



import { CommonModule, DatePipe } from '@angular/common';
import { DataTableModule } from 'app/d2kcomponents/data-table';
import { MatSelectSearchModule } from 'app/layout/mat-select-search/mat-select-search.module';
import { directiveModule } from './Directive/directive.module';
import { LogManagementModule } from './log-management/log-management.module';




const routes = [
    {
        path: 'dashboard',
        loadChildren: './home/home.module#HomeModule', canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    },
     {
        path: 'logmanagement',
        loadChildren: './log-management/log-management.module#LogManagementModule', canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    },
    {
        path: 'bankermapping',
        loadChildren: './bankerMapping/bankerMapping.module#bankerMappingModule', canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    }
    
    // {
    //     path: 'users',
    //     loadChildren: './SystemAdmin/SystemAdmin.module#SystemAdminModule', canActivate: [AuthGuard], canActivateChild: [AuthGuard]
    // },
  
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        ToastrModule.forRoot(),
        FuseSharedModule
        , BrowserAnimationsModule,
        MatDialogModule,
        CommonModule,
        RouterModule.forChild(routes),
        OrderModule,
        TranslateModule,
        MatCheckboxModule,
        MatSelectModule,
        FuseSharedModule,
        MatCardModule,
        MatIconModule,
        MatDialogModule,
        MatMenuModule,
        MatTabsModule,
        MatToolbarModule,
        MatDividerModule,
        NgxPaginationModule,
        MatButtonModule,
        MatExpansionModule,
        DataTableModule,
        MatChipsModule,
        MatSlideToggleModule,
        MatInputModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        FormsModule,
        MatSelectSearchModule,
        MatRadioModule,
        MatStepperModule,
        MatDatepickerModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        directiveModule,
        LogManagementModule,
        
    ],
    declarations: [],
    providers:[]
    
})
export class DashboardModule {
}
