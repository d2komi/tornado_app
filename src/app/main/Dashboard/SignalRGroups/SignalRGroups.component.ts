import { Component, OnInit } from '@angular/core';
import { HubConnectionBuilder } from '@aspnet/signalr';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { SignalRInstanceService, ConnectedUserData, LockUnlockApplication } from 'app/main/Singleton/SignalRInstance.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-SignalRGroups',
  templateUrl: './SignalRGroups.component.html',
  styleUrls: ['./SignalRGroups.component.css']
})
export class SignalRGroupsComponent implements OnInit {
  _hubConnection: any;
  Message: any;
  msgs: any[];

  ConnectedUser: ConnectedUserData;
  lockingApplication: LockUnlockApplication;

  userinfo = JSON.parse(localStorage.getItem('userinfo'));

  constructor(private toastr: ToastrService, private SignalRGlobalService: SignalRInstanceService) {
    this.ConnectedUser = new ConnectedUserData;
    this.lockingApplication = new LockUnlockApplication;
  }


  ngOnInit() {
    //SignalR Hub Connections
    if (this.userinfo != null) {
      this._hubConnection = new HubConnectionBuilder().withUrl(environment.HubURL).build();
      // To start connection 
      this._hubConnection.start().then(() => {
        console.log('MessageHub Connected');
        this.changeUserStatus(true);
        this.JoinSignalRGroups();
      });
    }
    if (this._hubConnection != undefined) {
      // After Group Join
      this._hubConnection.on('onJoinGroup', data => {
        var userifo = JSON.parse(localStorage.getItem('userinfo'));
        if (data.userid != userifo.userID) {
          console.log(data.userid + '  Join the Group');
          this.toastr.success(data.userid + ' Join The Group');
        }
      });

      // To LOCK and UnLock Application
      this._hubConnection.on('lockApplication', data => {
        this.lockingApplication.applicationID = data.appid;
        this.lockingApplication.isBlock = data.lockType;
        this.SignalRGlobalService.ChangeLockUnlockApp(this.lockingApplication);

      });

      // send method will get messgae 
      this._hubConnection.on('send', data => {
        console.log(this.Message);
      });

      //On Connection Close
      this._hubConnection.onclose(err => {
        this.userinfo = JSON.parse(localStorage.getItem('userinfo'));
        if (this.userinfo != null) {
          this.toastr.error('Reconnecting....');
        }
        this.changeUserStatus(false);
        this.Reconnect();
      });

    }



  }

  JoinSignalRGroups() {
    //To join SignalR Groups
    if (this.userinfo != null) {
      this._hubConnection
        .invoke('joinToGroup', this.userinfo.userID, this.userinfo.userGroup)
        .then(() => {
          this.toastr.success('YOU HAVE JOINED ELS GROUPS !!!');
        })
        .catch((err) =>{
          console.log(err);
        } );
    }

  }

  // Reconnect loop
  Reconnect() {
    this.userinfo = JSON.parse(localStorage.getItem('userinfo'));
    if (this.userinfo != null) {
      this._hubConnection.start()
        .then(() => {
          this.changeUserStatus(true);
          this.toastr.success('Reconnected');
          this.JoinSignalRGroups();
        })
        .catch((err) => {
          this.toastr.error('Reconnection Failed');
          this.changeUserStatus(false);
          console.log('Reconnection Failed');
          this.Reconnect();
        });
    }
  }

  changeUserStatus(isOnline) {
    if (this.userinfo != null) {
      this.ConnectedUser.UserName = this.userinfo.userID;
      this.ConnectedUser.ConnectionStatus = isOnline;
      this.SignalRGlobalService.ChangeUserStatus(this.ConnectedUser);
    }
  }

  ngOnDestroy() {
    if (this._hubConnection != undefined) {
      this._hubConnection.stop().then(() => {
        console.log('You Are disconnected and logout from Chat Server');
      });
    }

  }

}
