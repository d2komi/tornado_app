import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ReplaySubject, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FileManagerService } from '../../file-manager.service';
import { bankerMappingService } from '../../../bankerMapping.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { GlobalService, ImportMapping } from 'app/main/Dashboard/Global.service';

interface BankMaster {
entityKey: string;
mappingAllow: string;
masterTabaleName: string;
tableName: string;
}
@Component({
    selector: 'file-manager-details-sidebar',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss'],
    animations: fuseAnimations
})
export class FileManagerDetailsSidebarComponent implements OnInit, OnDestroy {
    selected: any;
    importForm: FormGroup;
    // Private
    private _unsubscribeAll: Subject<any>;
    _ImportMapping: ImportMapping;

    /**
     * Constructor
     *
     * @param {FileManagerService} _fileManagerService
     */
    constructor(
        private _fileManagerService: FileManagerService,
        private _formBuilder: FormBuilder,
        private _bankerMappingService: bankerMappingService,
        private _GlobalService: GlobalService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this._ImportMapping = new ImportMapping;

        // subscribe to home component messages
        this._GlobalService.ImportMappingData.subscribe(message => {
            // --exec ClientProjTabaleList @Flag='CP'
            // --exec ClientProjTabaleList @Flag='T',@ClientName='TMB',@ProjName='EWS'
            // --exec ClientProjTabaleList @Flag='D',@ClientName='TMB',@ProjName='EWS',@TableName='DimOccupation'
            this.ImportMappingFromSource("T", message.BankName, message.projectName, null);

        });
    }

    /** list of banks and projects */
    private banks: BankMaster[] = [];
    /** control for the MatSelect filter keyword */
    public bankFilterCtrl: FormControl = new FormControl();
    /** list of banks filtered by search keyword */
    public filteredBanks: ReplaySubject<BankMaster[]> = new ReplaySubject<BankMaster[]>(1);

    @ViewChild('bankList') bankList: MatSelect;

    /** Subject that emits when the component has been destroyed. */
    private _onDestroy = new Subject<void>();
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._fileManagerService.onFileSelected
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selected => {
                this.selected = selected;
            });

        this.importForm = this._formBuilder.group({
            bankName: ['', Validators.required],
            projectName: ['', Validators.required]
        });
        // this.ImportMappingFromSource("CP", null, null, null);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    ImportMappingFromSource(Flag, ClientName, ProjName, TableName) {
        this._bankerMappingService
            .ImportMappingFromSource(Flag, ClientName, ProjName, TableName)
            .subscribe(data => {
                let response = JSON.parse(data);
                response = response.data;
debugger;
                if (response.hasOwnProperty('masterNameList')) {
                    // load the initial bank list
                    this.banks = response.masterNameList;
                    this.filteredBanks.next(this.banks.slice());

                    // listen for search field value changes
                    this.bankFilterCtrl.valueChanges
                        .pipe(takeUntil(this._onDestroy))
                        .subscribe(() => {
                            this.filterBanks();
                        });
                }
            });
    }

    ngAfterViewInit() {
        // this.setInitialValue();
    }
    submitted() {
        if (this.importForm.invalid) {
            return;
        }

    }

    private filterBanks() {
        if (!this.banks) {
            return;
        }
        // get the search keyword
        let search = this.bankFilterCtrl.value;
        if (!search) {
            this.filteredBanks.next(this.banks.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanks.next(
            this.banks.filter(bank => bank.masterTabaleName.toLowerCase().indexOf(search) > -1)
        );
    }
}
