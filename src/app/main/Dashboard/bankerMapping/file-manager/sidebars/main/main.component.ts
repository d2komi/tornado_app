import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { GlobalService, ImportMapping } from 'app/main/Dashboard/Global.service';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { bankerMappingService } from '../../../bankerMapping.service';
interface Bank {
    clientName: string;
    tableName: string;
}
interface BankProjects {
    clientName: string;
    tableName: string;
    projectName: string;
}
@Component({
    selector: 'file-manager-main-sidebar',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class FileManagerMainSidebarComponent {
    selected: any;
    importForm: FormGroup;
    _ImportMapping: ImportMapping;

    /** control for the selected bank */
    // public bankCtrl: FormControl = new FormControl();

    /** control for the MatSelect filter keyword */
    public bankFilterCtrl: FormControl = new FormControl();
    public projectFilterCtrl: FormControl = new FormControl();


    constructor(private _GlobalService: GlobalService,private _formBuilder: FormBuilder, private _fuseSidebarService: FuseSidebarService, private _bankerMappingService: bankerMappingService) {

        this._ImportMapping = new ImportMapping;
     }

    /** list of banks and projects */
    private banks: Bank[] = [];
    private bankProjects: BankProjects[] = [];


    /** list of banks filtered by search keyword */
    public filteredBanks: ReplaySubject<Bank[]> = new ReplaySubject<Bank[]>(1);
    public filteredBanksProjects: ReplaySubject<BankProjects[]> = new ReplaySubject<BankProjects[]>(1);

    @ViewChild('bankList') bankList: MatSelect;
    @ViewChild('projectList') projectList: MatSelect;


    /** Subject that emits when the component has been destroyed. */
    private _onDestroy = new Subject<void>();
    ngOnInit(): void {
        this.importForm = this._formBuilder.group({
            bankName: ['', Validators.required],
            projectName: ['', Validators.required]
        });
        this.ImportMappingFromSource("CP", null, null, null);
        // set initial selection
        // this.bankCtrl.setValue(this.banks[10]);


    }

    ImportMappingFromSource(Flag, ClientName, ProjName, TableName) {
        this._bankerMappingService
            .ImportMappingFromSource(Flag, ClientName, ProjName, TableName)
            .subscribe(data => {
                let response = JSON.parse(data);
                response = response.data;

                if (response.hasOwnProperty('bankNameList')) {
                    // load the initial bank list
                    this.banks = response.bankNameList;
                    this.filteredBanks.next(this.banks.slice());

                    // listen for search field value changes
                    this.bankFilterCtrl.valueChanges
                        .pipe(takeUntil(this._onDestroy))
                        .subscribe(() => {
                            this.filterBanks();
                        });
                }
                if (response.hasOwnProperty('bankProjectList')) {
                    // load the initial Project list
                    this.bankProjects = response.bankProjectList;
                    this.filteredBanksProjects.next(this.bankProjects.slice());

                    // listen for search field value changes
                    this.projectFilterCtrl.valueChanges
                        .pipe(takeUntil(this._onDestroy))
                        .subscribe(() => {
                            this.filterBanksProject();
                        });
                }


            });
    }

    ngAfterViewInit() {
        // this.setInitialValue();
    }
    submitted() {
        if (this.importForm.invalid) {
            return;
        }
        this._fuseSidebarService.getSidebar('file-manager-main-sidebar').close();
        this._ImportMapping.BankName = this.importForm.value.projectName.clientName;
        this._ImportMapping.projectName = this.importForm.value.projectName.projectName;
        this._GlobalService.ImportMappingDataGetSet(this._ImportMapping);
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    private filterBanks() {
        if (!this.banks) {
            return;
        }
        // get the search keyword
        let search = this.bankFilterCtrl.value;
        if (!search) {
            this.filteredBanks.next(this.banks.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanks.next(
            this.banks.filter(bank => bank.clientName.toLowerCase().indexOf(search) > -1)
        );
    }
    private filterBanksProject() {
        if (!this.bankProjects) {
            return;
        }
        // get the search keyword
        let search = this.projectFilterCtrl.value;
        if (!search) {
            this.filteredBanksProjects.next(this.bankProjects.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanksProjects.next(
            this.bankProjects.filter(bank => bank.projectName.toLowerCase().indexOf(search) > -1)
        );
    }
}
