import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatRippleModule, MatSelect, MatSelectModule, MatSlideToggleModule, MatTableModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { FileManagerComponent } from './file-manager.component';
import { FileManagerService } from './file-manager.service';
import { FileManagerFileListComponent } from './file-list/file-list.component';
import { FileManagerDetailsSidebarComponent } from './sidebars/details/details.component';
import { FileManagerMainSidebarComponent } from './sidebars/main/main.component';
import { MatSelectSearchModule } from 'app/layout/mat-select-search/mat-select-search.module';
import { GlobalService } from '../../Global.service';


// import { FileManagerService } from 'app/main/apps/file-manager/file-manager.service';
// import { FileManagerComponent } from 'app/main/apps/file-manager/file-manager.component';
// import { FileManagerDetailsSidebarComponent } from 'app/main/apps/file-manager/sidebars/details/details.component';
// import { FileManagerFileListComponent } from 'app/main/apps/file-manager/file-list/file-list.component';
// import { FileManagerMainSidebarComponent } from 'app/main/apps/file-manager/sidebars/main/main.component';

const routes: Routes = [
    {
        path     : '**',
        component: FileManagerComponent,
        children : [],
        resolve  : {
            files: FileManagerService
        }
    }
];

@NgModule({
    declarations: [
        FileManagerComponent,
        FileManagerFileListComponent,
        FileManagerMainSidebarComponent,
        FileManagerDetailsSidebarComponent,
        
    ],
    imports     : [
        RouterModule.forChild(routes),
        MatButtonModule,
        MatIconModule,
        MatRippleModule,
        MatSlideToggleModule,
        MatTableModule,
        MatFormFieldModule, 
        MatInputModule,
        FuseSharedModule,
        FuseSidebarModule,
        MatSelectModule,
        MatSelectSearchModule
      
    ],
    providers   : [
        FileManagerService,
        GlobalService
    ]
})
export class FileManagerModule
{
}
