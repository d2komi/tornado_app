import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestMethod } from '@angular/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApiHandler } from '../../../AUTH/providers/api-handler.service';
import { isDefined } from '@angular/compiler/src/util';
// import { IUserCreation } from './IUserCreation';
@Injectable()
export class bankerMappingService {
  constructor(private ngxService: NgxUiLoaderService, private _apiHandler: ApiHandler) {
  }

  ImportMappingFromSource(Flag,ClientName,ProjName,TableName): Observable<string> {

    this.ngxService.start();
    return this._apiHandler.callService('/api/BankerMapping/ImportMappingFromSource/' + Flag+"/"+ClientName+"/"+ProjName+"/"+TableName, RequestMethod.Get)
      .map(res => <string>res.text())
      .do((result: string) => {
        this.ngxService.stop();
      });
  }

  handle(error: any): any {
    this.ngxService.stop();
    if (error.status === 401) {
    }
  }

}