import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatSortModule, MatPaginatorModule, MatInputModule, MatButtonModule, MatTabsModule, MatMenuModule, MatDialogModule, MatIconModule, MatCardModule, MatFormFieldModule, MatToolbarModule } from '@angular/material';
import { DataTableModule } from 'app/d2kcomponents/data-table';
import { bankerMappingService } from './bankerMapping.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { MasterMappingComponent } from './master-mapping/master-mapping.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    path: 'mastermapping',
    component: MasterMappingComponent // , canActivate: [RoleGuard]
  },
  {
    path        : 'file-manager',
    loadChildren: './file-manager/file-manager.module#FileManagerModule'
  }

  // loadChildren: './file-manager/file-manager.module#FileManagerModule'
 
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FuseSharedModule,

    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatTabsModule,
    MatButtonModule,
    DataTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
  ],
  providers: [bankerMappingService],
  declarations: [MasterMappingComponent]
})
export class bankerMappingModule { }
