import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { AuthGuard } from 'app/AUTH/guards/auth.guard';
import { RouterModule } from '@angular/router';
const routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
      path: 'home',
      component: HomeComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent],
  exports: [
    HomeComponent
]
})
export class HomeModule { }
