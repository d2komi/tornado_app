import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Pipe, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseUtils } from '@fuse/utils';
import { DataSource } from '@angular/cdk/table';
import { ToastrService } from 'ngx-toastr';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators, FormControl, FormArray } from '@angular/forms';
import { Subject, ReplaySubject, merge, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil, map, tap } from 'rxjs/operators';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';
import { isDefined } from '@angular/compiler/src/util';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { LogManagementService } from '../log-management.service';
import { CollectionViewer } from '@angular/cdk/collections';

@Component({
  selector: 'app-error-list',
  templateUrl: './error-list.component.html',
  styleUrls: ['./error-list.component.scss'],
  animations: fuseAnimations
})
export class ErrorListComponent implements OnInit {
  dataSource: FilesDataSource | null;
  displayedColumns = ['REF_COL_NAME', 'REF_COL_VALUE', 'ERRORCOLUMNNAME', 'ERRORCOLUMNDATA'];
  FileName: any;
  UserId: any;
  ErrorList: any[] = [];
  private _onDestroy = new Subject<void>();
  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  private _unsubscribeAll: Subject<any>;

  constructor(private _logManagementService: LogManagementService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.FileName = this.route.snapshot.paramMap.get('FileName');

    this.dataSource = new FilesDataSource([], this.paginator, this.sort);

    if (localStorage.userinfo != null) {
      let data = JSON.parse(localStorage.userinfo);
      this.UserId = data.userID;
    }
    this.serchlist(this.FileName)
  }
  serchlist(param) {
    this._logManagementService
      .GetErrorDisplayList(param)
      .subscribe(data => {
        let response = JSON.parse(data);
        response = response.data;

        if (response.hasOwnProperty('errorList')) {
          this.ErrorList = response.errorList;
        } else {
          this.ErrorList = [];
        }
        this.dataSource = new FilesDataSource(this.ErrorList, this.paginator, this.sort);

      });
  }
}
export class FilesDataSource extends DataSource<any>
{
  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject('');

  /**
   * Constructor
   *
   * @param {EcommerceProductsService} _ecommerceProductsService
   * @param {MatPaginator} _matPaginator
   * @param {MatSort} _matSort
   */
  constructor(
    private recipeData,
    private _matPaginator: MatPaginator,
    private _matSort: MatSort
  ) {
    super();

    this.filteredData = this.recipeData;
  }

  /**
   * Connect function called by the table to retrieve one stream containing the data to render.
   *
   * @returns {Observable<any[]>}
   */
  connect(): Observable<any[]> {
    const displayDataChanges = [
      this._matPaginator.page,
      this._filterChange,
      this._matSort.sortChange
    ];

    return merge(...displayDataChanges)
      .pipe(
        map(() => {
          let data = this.recipeData.slice();

          data = this.filterData(data);

          this.filteredData = [...data];

          data = this.sortData(data);

          // Grab the page's slice of data.
          const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
          return data.splice(startIndex, this._matPaginator.pageSize);
        }
        ));
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  // Filtered data
  get filteredData(): any {
    return this._filteredDataChange.value;
  }

  set filteredData(value: any) {
    this._filteredDataChange.next(value);
  }

  // Filter
  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter data
   *
   * @param data
   * @returns {any}
   */
  filterData(data): any {
    if (!this.filter) {
      return data;
    }
    return FuseUtils.filterArrayByString(data, this.filter);
  }

  /**
   * Sort data
   *
   * @param data
   * @returns {any[]}
   */
  sortData(data): any[] {
    if (!this._matSort.active || this._matSort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._matSort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'name':
          [propertyA, propertyB] = [a.name, b.name];
          break;
        case 'categories':
          [propertyA, propertyB] = [a.categories[0], b.categories[0]];
          break;
        case 'price':
          [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
          break;
        case 'quantity':
          [propertyA, propertyB] = [a.quantity, b.quantity];
          break;
        case 'active':
          [propertyA, propertyB] = [a.active, b.active];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
    });
  }


  /**
   * Disconnect
   */
  disconnect(): void {
  }
}