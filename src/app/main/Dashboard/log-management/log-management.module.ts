import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatSortModule, MatPaginatorModule, MatInputModule, MatButtonModule, MatTabsModule, MatMenuModule, MatDialogModule, MatIconModule, MatCardModule, MatFormFieldModule, MatToolbarModule } from '@angular/material';
import { DataTableModule } from 'app/d2kcomponents/data-table';
import { LogManagementService } from './log-management.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { ErrorListComponent } from './error-list/error-list.component';
import { LogListComponent } from './log-list/log-list.component';

const routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    path: 'loglist',
    component: LogListComponent // , canActivate: [RoleGuard]
  },
  {
    path: 'loglist/:FileName',
    component: ErrorListComponent // , canActivate: [RoleGuard]
  },
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FuseSharedModule,

    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatTabsModule,
    MatButtonModule,
    DataTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatFormFieldModule,
  ],
  providers: [LogManagementService],
  declarations: [LogListComponent, ErrorListComponent]
})
export class LogManagementModule { }
