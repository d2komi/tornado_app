import { LogManagementModule } from './log-management.module';

describe('LogManagementModule', () => {
  let logManagementModule: LogManagementModule;

  beforeEach(() => {
    logManagementModule = new LogManagementModule();
  });

  it('should create an instance', () => {
    expect(logManagementModule).toBeTruthy();
  });
});
