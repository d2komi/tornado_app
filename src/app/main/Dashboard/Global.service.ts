import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { ApiHandler } from '../../AUTH/providers/api-handler.service';
import { RequestMethod } from '@angular/http';
import { FormGroup } from '@angular/forms';


export class ConnectedUserData {
    public UserName: String = "";
    public ConnectionStatus: boolean;
}
export class ImportMapping {
    public BankName: String = "";
    public projectName: String = "";

}
export class LockUnlockApplication {
    public isBlock: boolean;
    public applicationID: number;
}

export class Filters {
    public loanTypes: string[];
    filters: any;
}

@Injectable()

export class GlobalService {

    //For SignalR Connection Status
    ConnectedUserData: Subject<ConnectedUserData> = new Subject();
    dateformat: string;


    get ConnectedUser(): Subject<ConnectedUserData> {
        return this.ConnectedUserData;
    }
    set ConnectedUser(src: Subject<ConnectedUserData>) {
        this.ConnectedUserData = src;
    }
    // for Application Lock unlock
    LockUnlockApplication: Subject<LockUnlockApplication> = new Subject();
    get LockUnlock(): Subject<LockUnlockApplication> {
        return this.LockUnlockApplication;
    }
    set LockUnlock(src: Subject<LockUnlockApplication>) {
        this.LockUnlockApplication = src;
    }

    // for add and remove Filters
    Filters: Subject<Filters> = new Subject();
    get FilterFields(): Subject<Filters> {
        return this.Filters;
    }
    set FilterFields(src: Subject<Filters>) {
        this.Filters = src;
    }
    // Import Mapping
    ImportMapping: Subject<ImportMapping> = new Subject();
    get ImportMappingData(): Subject<ImportMapping> {
        return this.ImportMapping;
    }
    set ImportMappingData(src: Subject<ImportMapping>) {
        this.ImportMapping = src;
    }

    constructor(private _apiHandler: ApiHandler) { }
    // Methods For Live data transfer 
    ChangeUserStatus(user: ConnectedUserData) {
        this.ConnectedUser.next(user);
    }
    ImportMappingDataGetSet(mapping: ImportMapping) {
        this.ImportMappingData.next(mapping);
    }
    ChangeLockUnlockApp(app: LockUnlockApplication) {
        this.LockUnlock.next(app);
    }

    FilterApplication(filter: Filters) {
        this.FilterFields.next(filter);
    }

    AutoSelect(form: FormGroup, List: any, ControlName: any, SetBy: any, SetValue: any) {
        if (List.length == 1) {
            if (SetBy == "ColumnName") {
                form.controls[ControlName].patchValue(List[0][SetValue]);
            } else if (SetBy == "Data") {
                form.controls[ControlName].patchValue(List[0]);
            }
        }
    }


    search(arr, s) {
        var matches = [], i, key;
        var lookup = {};
        var result = [];

        for (i = arr.length; i--;) {
            for (key in arr[i]) {
                if (key.toLowerCase() != 'tablename' && arr[i][key] != null) {
                    const Key = arr[i].hasOwnProperty(key);
                    var value = arr[i][key].toString();
                    value = value.toLowerCase().indexOf(s.toLowerCase().trim());

                    if (Key && value > -1) {
                        if (matches.length > 0) {
                            if (!containsObject(arr[i], matches)) {
                                matches.push(arr[i]);
                            }
                        } else {
                            matches.push(arr[i]);
                        }
                    }
                }
            }
        }
        function containsObject(obj, list) {
            var j;
            for (j = 0; j < list.length; j++) {
                if (list[j] === obj) {
                    return true;
                }
            }
            return false;
        }

        return matches;
    };

    ConvertDate_YYYYMMDD_Format(VarDate) {
        let date = VarDate.split('/');
        this.dateformat = date[1] + '/' + date[0] + '/' + date[2];
        return this.dateformat;
    }








}
