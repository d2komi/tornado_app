import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export class ConnectedUserData {
  public UserName: String = "";
  public ConnectionStatus: boolean;
}

export class LockUnlockApplication {
  public isBlock: boolean;
  public applicationID: number;
}

export class Filters {
  public loanTypes: string[];
  filters: any;
}

@Injectable({
  providedIn: 'root'
})



export class SignalRInstanceService {
  
    //For SignalR Connection Status
    ConnectedUserData: Subject<ConnectedUserData> = new Subject();
    get ConnectedUser(): Subject<ConnectedUserData> {
        return this.ConnectedUserData;
    }
    set ConnectedUser(src: Subject<ConnectedUserData>) {
        this.ConnectedUserData = src;
    }
    // for Application Lock unlock
    LockUnlockApplication: Subject<LockUnlockApplication> = new Subject();
    get LockUnlock(): Subject<LockUnlockApplication> {
        return this.LockUnlockApplication;
    }
    set LockUnlock(src: Subject<LockUnlockApplication>) {
        this.LockUnlockApplication = src;
    }

    // for add and remove Filters
    Filters: Subject<Filters> = new Subject();
    get FilterFields(): Subject<Filters> {
        return this.Filters;
    }
    set FilterFields(src: Subject<Filters>) {
        this.Filters = src;
    }

    constructor() { }
    // Methods For Live data transfer 
    ChangeUserStatus(user: ConnectedUserData) {
        this.ConnectedUser.next(user);
    }
    ChangeLockUnlockApp(app: LockUnlockApplication) {
        this.LockUnlock.next(app);
    }

    FilterApplication(filter: Filters) {
        this.FilterFields.next(filter);
    }

    search(arr, s) {
        var matches = [], i, key;
        var lookup = {};
        var result = [];

        for (i = arr.length; i--;) {
            for (key in arr[i]) {
                if (key != 'tableName' && arr[i][key]!=null) {
                    const Key = arr[i].hasOwnProperty(key);
                    var value = arr[i][key].toString();
                    value = value.toLowerCase().indexOf(s.toLowerCase().trim());

                    if (Key && value > -1) {
                        if (matches.length > 0) {
                            if (!containsObject(arr[i], matches)) {
                                matches.push(arr[i]);
                            }
                        }else{
                            matches.push(arr[i]);
                        }
                    }
                }
            }
        }
        function containsObject(obj, list) {
            var j;
            for (j = 0; j < list.length; j++) {
                if (list[j] === obj) {
                    return true;
                }
            }
            return false;
        }

        return matches;
    };

}
