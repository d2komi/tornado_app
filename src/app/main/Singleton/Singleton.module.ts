import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleInstanceService, ChangeViewModeConfig } from './SingleInstance.service';
import { SignalRInstanceService } from './SignalRInstance.service';

@NgModule({
  imports: [CommonModule],
  providers: [SingleInstanceService,SignalRInstanceService]
})
export class SingletonModule {
  constructor() {

  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SingletonModule,
    };
  }

}
