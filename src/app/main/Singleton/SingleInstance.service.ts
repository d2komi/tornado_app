import { Injectable, Optional } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
let nextId = 1;
@Injectable({
  providedIn: 'root'
})

export class ChangeViewModeConfig {
  public title: string;
  public Mode: string;
}
export class SearchTextConfig {
  public search: string;
}
export class SingleInstanceService {

  // for Change View Mode Type And Set Active Mode Button 
  ViewMode: Subject<ChangeViewModeConfig> = new Subject();
  get ChangeView(): Subject<ChangeViewModeConfig> {
    return this.ViewMode;
  }
  set ChangeView(src: Subject<ChangeViewModeConfig>) {
    this.ViewMode = src;
  }
  // For Global Search Data where List exist
  Search: Subject<SearchTextConfig> = new Subject();
  get SearchText(): Subject<SearchTextConfig> {
    return this.Search;
  }
  set SearchText(src: Subject<SearchTextConfig>) {
    this.Search = src;
  }

  constructor() {
  }

  ChangeViewModeList(filter: ChangeViewModeConfig) {
    this.ChangeView.next(filter);
  }

  DoSearch(filter: SearchTextConfig) {
    this.SearchText.next(filter);
  }

  minusYear(val) {
    // let dateparam = new Date().toISOString().split('T')[0];
    // let tempyear = dateparam.split('-')[0];
    // tempyear = (parseInt(tempyear, 10) - val).toString();

    // dateparam = tempyear + '-' + dateparam.split('-')[1] + '-' + dateparam.split('-')[2];
    var d = new Date();
    d.setFullYear(d.getFullYear() - val);
    return d;


  }
  minusMonth(val) {
    // let dateparam = new Date().toISOString().split('T')[0];
    // let tempMonth = dateparam.split('-')[1];
    // let Year = dateparam.split('-')[0];

    var d = new Date();
    d.setMonth(d.getMonth() - val);

    // tempMonth = (parseInt(tempMonth, 10) - val).toString();
    // if (Number(tempMonth) > 12) {
    //   tempMonth = (Number(tempMonth) - 12).toString();
    //   Year = (Number(Year) + 1).toString()
    // }
    // dateparam = Year + '-' + tempMonth + '-' + dateparam.split('-')[2];
    return d;
  }
  minusDate(val) {
    // let dateparam = new Date().toISOString().split('T')[0];
    // let tempDay = dateparam.split('-')[2];
    // tempDay = (parseInt(tempDay, 10) - val).toString();

    // dateparam = dateparam.split('-')[0] + '-' + dateparam.split('-')[1] + '-' + tempDay;

    var d = new Date();
    d.setDate(d.getDate() - val);
    return d;



  }

  calAge(DOB) {
    if (DOB) {
      var timeDiff = Math.abs(Date.now() - new Date(DOB).getTime());
      return Math.round(timeDiff / (1000 * 3600 * 24) / 365.25)
    }
  }
  resetForm(form: FormGroup) {

    form.reset();

    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }
  TouchedForm(form: FormGroup) {
    Object.keys(form.controls).forEach(key => {
      form.get(key).markAsTouched();
    });
  }
}
