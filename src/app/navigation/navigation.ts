import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'dashboards',
        title: 'TORNADO Dashboards',
        type: 'group',
        icon: 'apps',
        children: [
            // {
            //     id: 'dashboard',
            //     title: 'Dashboard',
            //     type: 'item',
            //     url: '/dashboard/home',
            //     icon: 'apps',
            // },
            {
                id: 'loglist',
                title: 'Log List',
                type: 'item',
                url: '/logmanagement/loglist',
                icon: 'apps',
            },
            // {
            //     id: 'logdetails',
            //     title: 'Log Details',
            //     type: 'item',
            //     url: '/logmanagement/Logdetails/',
            //     icon: 'apps',
            // }




        ]

    },
    {
    id: 'BankerMapping',
    title: 'Banker Mapping',
    type: 'collapsable',
    icon: 'chrome_reader_mode',
    children: [
        {
            id: 'MasterMapping',
            title: 'Master mapping',
            type: 'item',
            icon: 'chrome_reader_mode',
            url: '/bankermapping/mastermapping',
            exactMatch: false

        }]
    }

]


// =============
// {
//     id: 'Master Maintenance',
//     title: 'Master Maintenance',
//     type: 'collapsable',
//     icon: 'chrome_reader_mode',
//     children: [
//         {
//             id: 'Data Point Owner User',
//             title: 'Data Point Owner User',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: '/Masters/DataPointOwnerUser',
//             exactMatch: true


//         },
//         {
//             id: 'Tranche -I Master',
//             title: 'Tranche – I Data Point Master',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: '/Masters/TranceMaster',
//             exactMatch: true


//         }
//     ]
// },


// {
//     id: 'Operation',
//     title: 'Operation',
//     type: 'collapsable',
//     icon: 'work_outline',
//     children: [
//         {
//             id: 'Re-Assign Data Point Owners',
//             title: 'Re-Assign Data Point Owners',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: '/Operations/ReassignDataPointOwners',
//             exactMatch: true
//         },
//         {
//             id: 'Processing',
//             title: 'Processing',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: 'Operations/Processing',
//             exactMatch: true

//         },
//         {
//             id: 'Manual Data Points Entry',
//             title: 'Manual Data Points Entry',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: 'Operations/ManualDataPointsEntry',
//             exactMatch: true

//         },
//         {
//             id: 'Adjustments',
//             title: 'Adjustments',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: 'Operations/Adjustments',
//             exactMatch: true

//         },
//         {
//             id: 'ExceptionalVariationAuthorization',
//             title: 'Exceptional Variation Authorization',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: 'Operations/ExceptionalVariationAuthorization',
//             exactMatch: true

//         },

//         {
//             id: 'Data Point Output View/Export',
//             title: 'Data Point Output View/Export',
//             type: 'item',
//             icon: 'chrome_reader_mode',
//             url: 'Operations/DataPointOutputViewExport',
//             exactMatch: true
//         }
//     ]
// },

// {
//     id: 'Reports',
//     title: 'RBS Report',
//     type: 'item',
//     url: '/Operations/Reports',
//     icon: 'track_changes',
// },
// {
//     id: 'RBI Freeze',
//     title: 'RBI Freeze',
//     type: 'item',
//     url: '/Freeze',
//     icon: 'track_changes',
// }