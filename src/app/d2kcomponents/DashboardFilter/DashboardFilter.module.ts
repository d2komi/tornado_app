import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter/filter.component';
import { ApplicationFilterPipe } from './filter/applicationFilter.pipe';
import { MatIconModule, MatDialogModule, MatMenuModule, MatTabsModule, MatToolbarModule, MatDividerModule, MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
    MatDividerModule,
    NgxPaginationModule,
    MatButtonModule

  ],
  declarations: [FilterComponent,ApplicationFilterPipe],
  exports:[FilterComponent,ApplicationFilterPipe]

})
export class DashboardFilterModule { }
