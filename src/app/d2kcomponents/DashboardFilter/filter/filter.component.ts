import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { SignalRInstanceService } from 'app/main/Singleton/SignalRInstance.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  filterFormGroup: FormGroup
  filters: any;
  selected: any;

  constructor(private formBuilder: FormBuilder, private SignalRGlobalService: SignalRInstanceService) {
  }


  ngOnInit() {

    this.filterFormGroup = this.formBuilder.group({
      filters: this.formBuilder.array([])
    });


    setTimeout((res) => {
      this.filters = [{ title: "Home Loans- Repair & Renovate", field: "RepairRenovate" },
      { title: "Home Loans- Transfer Existing", field: "TransferExistingHomeLoan" },
      { title: "Education loans", field: "EducationLoan" },
      { title: "Vehicle Loans", field: "VehicleLoan" },
      { title: "Gold loans", field: "GoldLoan" }
      ];
    });

  }
  onChange(event) {
    const selectedFilters = <FormArray>this.filterFormGroup.get('filters') as FormArray;
    if (event.checked) {
      selectedFilters.push(new FormControl(event.source.value))
    } else {
      const i = selectedFilters.controls.findIndex(x => x.value === event.source.value);
      selectedFilters.removeAt(i);
    }
    this.submit();
  }
  submit() {
    this.SignalRGlobalService.FilterApplication(this.filterFormGroup.value);
    console.log(this.filterFormGroup.value);
  }

}
