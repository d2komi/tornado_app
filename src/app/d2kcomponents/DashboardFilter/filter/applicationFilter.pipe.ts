import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'applicationFilter'
})
export class ApplicationFilterPipe implements PipeTransform {

  transform(applications: any[], selectedFilters: string[]): any[] {
    const selectedFilter = selectedFilters;
    if (!applications) return [];
    if (!selectedFilter || selectedFilter.length === 0) return applications;
    return applications.filter(app => {
      let applicationPresent = false;
      selectedFilter.forEach(id => {
        if (app.loanType === id) {
          applicationPresent = true;
        }
      });
      return applicationPresent;
    });
  }

}
